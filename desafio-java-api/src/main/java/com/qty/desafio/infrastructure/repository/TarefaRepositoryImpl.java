package com.qty.desafio.infrastructure.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.qty.desafio.domain.model.Tarefa;
import com.qty.desafio.domain.repository.TarefaRepository;

@Component
public class TarefaRepositoryImpl implements TarefaRepository {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<Tarefa> listar() {
		return entityManager.createQuery("from Tarefa", Tarefa.class).getResultList();
	}
	
	@Override
	public Tarefa buscar(Integer id) {
		return entityManager.find(Tarefa.class, id);
	}
	
	@Transactional
	@Override
	public Tarefa salvar(Tarefa tarefa) {
		return entityManager.merge(tarefa);
	}
	
	@Transactional
	@Override
	public void remover(Integer id) {
		Tarefa tarefa = buscar(id);
		if (tarefa == null) {
			throw new EmptyResultDataAccessException(1);
		}
		entityManager.remove(tarefa);
	}

}

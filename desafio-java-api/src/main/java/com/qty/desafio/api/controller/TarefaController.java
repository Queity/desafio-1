package com.qty.desafio.api.controller;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.qty.desafio.domain.exception.EntidadeEmUsoException;
import com.qty.desafio.domain.exception.EntidadeNaoEncontradaException;
import com.qty.desafio.domain.model.Tarefa;
import com.qty.desafio.domain.repository.TarefaRepository;
import com.qty.desafio.domain.service.CadastroTarefaService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/tarefas")
public class TarefaController {

	@Autowired
	private TarefaRepository tarefaRepository;
	
	@Autowired
	private CadastroTarefaService cadastroTarefaService;
	
	@GetMapping
	public List<Tarefa> listar() {
		return tarefaRepository.listar();
	}
	
	@GetMapping("/{idTarefa}")
	public ResponseEntity<Tarefa> buscar(@PathVariable Integer idTarefa) {
		Tarefa tarefa = tarefaRepository.buscar(idTarefa);
		
		if (tarefa != null) {
			return ResponseEntity.ok(tarefa);
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Tarefa adicionar(@RequestBody Tarefa tarefa) {
		return cadastroTarefaService.salvar(tarefa);
	}
	
	@PutMapping("/{idTarefa}")
	public ResponseEntity<Tarefa> atualizar(@PathVariable Integer idTarefa, @RequestBody Tarefa tarefa) {
		Tarefa tarefaAtual = tarefaRepository.buscar(idTarefa);
		
		if (tarefaAtual != null) {
			BeanUtils.copyProperties(tarefa, tarefaAtual, "id");
			tarefaAtual = cadastroTarefaService.salvar(tarefaAtual);
			return ResponseEntity.ok(tarefaAtual);
		}
		
		
		return ResponseEntity.notFound().build();
	}

	@DeleteMapping("/{idTarefa}")
	public ResponseEntity<Tarefa> remover(@PathVariable Integer idTarefa) {
		try {
			cadastroTarefaService.excluir(idTarefa);
			return ResponseEntity.noContent().build();
		} catch (EntidadeNaoEncontradaException e) {
			return ResponseEntity.notFound().build();
		} catch (EntidadeEmUsoException e) {
			return ResponseEntity.status(HttpStatus.CONFLICT).build();
		}
	}
	
}

package com.qty.desafio.domain.repository;

import java.util.List;

import com.qty.desafio.domain.model.Tarefa;

public interface TarefaRepository {

	List<Tarefa> listar();
	Tarefa buscar(Integer id);
	Tarefa salvar(Tarefa tarefa);
	void remover(Integer id);

}

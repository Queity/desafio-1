package com.qty.desafio.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.qty.desafio.domain.exception.EntidadeEmUsoException;
import com.qty.desafio.domain.exception.EntidadeNaoEncontradaException;
import com.qty.desafio.domain.model.Tarefa;
import com.qty.desafio.domain.model.TarefaItem;
import com.qty.desafio.domain.repository.TarefaItemRepository;
import com.qty.desafio.domain.repository.TarefaRepository;

@Service
public class CadastroTarefaItemService {

	@Autowired
	private TarefaItemRepository tarefaItemRepository;
	
	@Autowired
	private TarefaRepository tarefaRepository;
	
	public TarefaItem salvar(TarefaItem tarefaItem) {
		Integer idTarefa = tarefaItem.getTarefa().getId();
		Tarefa tarefa = tarefaRepository.buscar(idTarefa);
		
		if (tarefa == null) {
			throw new EntidadeNaoEncontradaException(
				String.format("Não existe cadastro de tarefa com código %d", idTarefa));
		}
		
		tarefaItem.setTarefa(tarefa);
		
		return tarefaItemRepository.salvar(tarefaItem);
	}
	
	public void excluir(Integer idItem) {
		try {
			tarefaItemRepository.remover(idItem);
		} catch (EmptyResultDataAccessException e) {
			throw new EntidadeNaoEncontradaException (
				String.format("Não existe um cadastro de item com código %d", idItem));
		} catch (DataIntegrityViolationException e) {
			throw new EntidadeEmUsoException (
				String.format("Item de código %d não pode ser removida, pois está em uso", idItem));
		}
	}
	
}

import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable} from 'rxjs';

import { TarefaItem } from './model/tarefa.item';

@Injectable()
export class TarefaItemService {

    private baseUrlService: string;

    constructor(private http: HttpClient) {
        this.baseUrlService = 'http://localhost:8080/itens';
    }

    public listar(): Observable<TarefaItem[]> {
        return this.http.get<TarefaItem[]>(this.baseUrlService);
    }

    public buscar(id: number) {
        return this.http.get<TarefaItem>(this.baseUrlService +"/"+ id);
    }

    public adicionar(tarefaItem: TarefaItem) {
        return this.http.post<TarefaItem>(this.baseUrlService, tarefaItem);
    }

    public atualizar(tarefaItem: TarefaItem) {
        return this.http.put<TarefaItem>(this.baseUrlService, tarefaItem);
    }

    public remover(id: number) {
        return this.http.delete<TarefaItem>(this.baseUrlService +"/"+ id);
    }

}
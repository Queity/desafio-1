import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable} from 'rxjs';

import { Tarefa } from './model/tarefa';

@Injectable()
export class TarefaService {

    private baseUrlService: string;

    constructor(private http: HttpClient) {
        this.baseUrlService = 'http://localhost:8080/tarefas';
    }

    public listar(): Observable<Tarefa[]> {
        return this.http.get<Tarefa[]>(this.baseUrlService);
    }

    public buscar(id: number) {
        return this.http.get<Tarefa>(this.baseUrlService +"/"+ id);
    }

    public adicionar(tarefa: Tarefa) {
        return this.http.post<Tarefa>(this.baseUrlService, tarefa);
    }

    public atualizar(tarefa: Tarefa) {
        return this.http.put<Tarefa>(this.baseUrlService, tarefa);
    }

    public remover(id: number) {
        return this.http.delete<Tarefa>(this.baseUrlService +"/"+ id);
    }

}
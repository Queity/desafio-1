import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TarefaCadastroComponent } from './tarefa/cadastro/tarefa-cadastro.component';
import { TarefaConsultaComponent } from './tarefa/consulta/tarefa-consulta.component';
import { TarefaItemConsultaComponent } from './tarefa-item/consulta/tarefa-item-consulta.component';
import { TarefaItemCadastroComponent } from './tarefa-item/cadastro/tarefa-item-cadastro.component';
import { TarefaItemDeleteComponent } from './tarefa-item/delete/tarefa-item-delete.component';

const routes: Routes = [
  { path: 'consulta-tarefa', component: TarefaConsultaComponent },
  { path: 'cadastro-tarefa', component: TarefaCadastroComponent },
  { path: 'consulta-tarefa-item', component: TarefaItemConsultaComponent },
  { path: 'cadastro-tarefa-item', component: TarefaItemCadastroComponent },
  { path: 'deletar-tarefa-item/:idItem', component: TarefaItemDeleteComponent },
  { path: 'cadastro-tarefa-item/:idTarefa', component: TarefaItemCadastroComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { TarefaConsultaComponent } from './tarefa/consulta/tarefa-consulta.component';
import { TarefaCadastroComponent } from './tarefa/cadastro/tarefa-cadastro.component';
import { TarefaItemConsultaComponent } from './tarefa-item/consulta/tarefa-item-consulta.component';
import { TarefaItemCadastroComponent } from './tarefa-item/cadastro/tarefa-item-cadastro.component';
import { TarefaItemDeleteComponent } from './tarefa-item/delete/tarefa-item-delete.component';

import { TarefaService } from './services/tarefa.service';
import { TarefaItemService } from './services/tarefa.item.service';

@NgModule({
  declarations: [
    AppComponent,
    TarefaConsultaComponent,
    TarefaCadastroComponent,
    TarefaItemConsultaComponent,
    TarefaItemCadastroComponent,
    TarefaItemDeleteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [TarefaService,TarefaItemService],
  bootstrap: [AppComponent]
})
export class AppModule { }

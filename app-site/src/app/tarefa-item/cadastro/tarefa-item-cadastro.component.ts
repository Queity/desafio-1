import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TarefaItemService } from '../../services/tarefa.item.service';
import { TarefaItem } from '../../services/model/tarefa.item';
import { Tarefa } from 'src/app/services/model/tarefa';

@Component({
    selector: 'app-cadastro-tarefa-item',
    templateUrl: './tarefa-item-cadastro.component.html',
    styleUrls: ['./tarefa-item-cadastro.component.css']
})
export class TarefaItemCadastroComponent {

    item: TarefaItem;

    constructor(private route: ActivatedRoute, private router: Router, private tarefaItemService: TarefaItemService) {
        this.item = new TarefaItem();

        this.route.params.subscribe(parametro=>{
            if(parametro['idTarefa'] != undefined) {
                this.item.tarefa = new Tarefa();
                this.item.tarefa.id = Number(parametro['idTarefa']);
            }
        })
    }

    onSubmit() {
        this.tarefaItemService.adicionar(this.item).subscribe(result => this.gotoTarefaList());
    }

    gotoTarefaList() {
        this.router.navigate(['/consulta-tarefa-item']);
    }

}
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TarefaItemService } from '../../services/tarefa.item.service';
import { TarefaItem } from '../../services/model/tarefa.item';

@Component({
    selector: 'app-consulta-tarefa-item',
    templateUrl: './tarefa-item-consulta.component.html',
    styleUrls: ["./tarefa-item-consulta.component.css"]
})
export class TarefaItemConsultaComponent implements OnInit {

    itens: TarefaItem[];

    constructor(private route: ActivatedRoute, private router: Router,private tarefaItemService: TarefaItemService) {
    }

    ngOnInit() {
        this.tarefaItemService.listar().subscribe(data => {
            this.itens = data;
        });
    }

    deletarItem(idItem:number):void{
        this.router.navigate(['/deletar-tarefa-item',idItem]);
    }

}